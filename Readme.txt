This will eventually be a game. At the moment it rather is a wild construction. But hey, there is a caterpillar and it moves.

hints for installation:

- make a build directory somewhere
- run cmake with the adress to the source directory as first parameter
- change directory to the source directory and run ./HatHunter in the build directory from there

hints for playing the game:

- you can use the four arrow keys and the shift keys for movement
- the shift key is not intuitive, it is for border cases, where you want to move the back of the caterpillar before it is stretched
