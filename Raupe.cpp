#include "Raupe.hpp"

Raupe::Raupe(SDL_Renderer *renderer,
		double *LevelScale,
		int *Offsetx, int *Offsety,
		int *WindowW, int *WindowH){
	HeadTexture=NULL;
	TailTexture=NULL;
	BodyPartTexture=NULL;
	ScaleL=LevelScale;
	OffsetLx=Offsetx;
	OffsetLy=Offsety;
	WindowWidth=WindowW;
	WindowHight=WindowH;
	xb=80;
	yb=100;
	xf=180;
	yf=100;
	movingLtR=false;
	movingRtL=false;
	movingf=true;
	movingUp=false;
	movingDown=false;
	userswitched=false;
	length=8;
	x=(int *) malloc(length*sizeof(int));
	y=(int *) malloc(length*sizeof(int));
	for (int i=0;i<length;i++){
		x[i]=xb;
		y[i]=yb;
	}
	velocity=8;
	framesWaited=0;
	Twidth=16;
	Thight=16;
	LoadTexture(renderer);
}
Raupe::~Raupe(){
	//free(x);
	//free(y);
	free();
}
Raupe::Raupe(SDL_Renderer *renderer,
		double *LevelScale,
		int *Offsetx, int *Offsety,
		int *WindowW, int *WindowH,
		int x1, int y1, int x2, int y2, int l){
	HeadTexture=NULL;
	TailTexture=NULL;
	BodyPartTexture=NULL;
	ScaleL=LevelScale;
	OffsetLx=Offsetx;
	OffsetLy=Offsety;
	WindowWidth=WindowW;
	WindowHight=WindowH;
	xb=x1;
	yb=y1;
	xf=x2;
	yf=y2;
	movingLtR=false;
	movingRtL=false;
	movingf=true;
	movingUp=false;
	movingDown=false;
	userswitched=false;
	length=l;
	x=(int *) malloc(length*sizeof(int));
	y=(int *) malloc(length*sizeof(int));
	for (int i=0;i<length;i++){
		x[i]=xb;
		y[i]=yb;
	}
	velocity=16;
	framesWaited=0;
	Twidth=16;
	Thight=16;
	LoadTexture(renderer);
}
void Raupe::free()
{
	//Free texture if it exists
	if( HeadTexture != NULL )
	{
		SDL_DestroyTexture( HeadTexture );
		HeadTexture = NULL;
	}
	if( BodyPartTexture != NULL )
	{
		SDL_DestroyTexture( BodyPartTexture );
		BodyPartTexture = NULL;
	}
	if( TailTexture != NULL )
	{
		SDL_DestroyTexture( TailTexture );
		TailTexture = NULL;
	}
}
bool Raupe::LoadHeadTexture(SDL_Renderer *renderer)
{
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load("img/Head.png");
	if( loadedSurface == NULL )
	{
		printf("Unable to load image img/Head.png! SDL_image Error: %s\n", 
				IMG_GetError() );
	}
	else
	{
		//Create texture from surface pixels
        	newTexture = SDL_CreateTextureFromSurface( renderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf("Unable to create texture from img/Head.png! SDL Error: %s\n"
					, SDL_GetError() );
		}
		
		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	//Return success
	HeadTexture = newTexture;
	return HeadTexture != NULL;
}
bool Raupe::LoadBodyPartTexture(SDL_Renderer *renderer)
{
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load("img/BodyPart.png");
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image img/BodyPart.png! SDL_image Error: %s\n",
				IMG_GetError() );
	}
	else
	{
		//Create texture from surface pixels
        	newTexture = SDL_CreateTextureFromSurface( renderer, loadedSurface );
		if( newTexture == NULL )
		{
		printf("Unable to create texture from img/BodyPart.png! SDL Error: %s\n"
				, SDL_GetError() );
		}
		
		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	//Return success
	BodyPartTexture = newTexture;
	return BodyPartTexture != NULL;
}
bool Raupe::LoadTailTexture(SDL_Renderer *renderer)
{
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load("img/Tail.png");
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image img/Tail.png! SDL_image Error: %s\n", 
				IMG_GetError() );
	}
	else
	{
		//Create texture from surface pixels
        	newTexture = SDL_CreateTextureFromSurface( renderer, loadedSurface );
		if( newTexture == NULL )
		{
		printf( "Unable to create texture from img/Tail.png! SDL Error: %s\n",
				SDL_GetError() );
		}
		
		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	//Return success
	TailTexture = newTexture;
	return TailTexture != NULL;
}
void Raupe::LoadTexture(SDL_Renderer *renderer){
	free();
	LoadHeadTexture(renderer);
	LoadBodyPartTexture(renderer);
	LoadTailTexture(renderer);
}
/*collision front*/
bool Raupe::CollisionFront(){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*xf))/W;
	int i=(int)((double)(h*yf))/H; 
	int j1=(int)((double)(w*(xf+Twidth)))/W;
	int i1=(int)((double)(h*(yf+Thight)))/H; 
	
	if((*colliderMatrix)[j+i*w]==true||
		(*colliderMatrix)[j1+i*w]==true||
		(*colliderMatrix)[j+i1*w]==true||
		(*colliderMatrix)[j1+i1*w]==true			
			){
		return true;
	}else{
		return false;
	}
}
bool Raupe::CollisionFrontU(){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*xf+1))/W;
	int i=(int)((double)(h*yf-1))/H;//why -1
	int j1=(int)((double)(w*(xf+Twidth-1)))/W;
	
	return((*colliderMatrix)[j+i*w]==true||
		(*colliderMatrix)[j1+i*w]==true);
}
bool Raupe::CollisionFrontD(){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*xf+1))/W;
	int j1=(int)((double)(w*(xf+Twidth-1)))/W;
	int i1=(int)((double)(h*(yf+Thight)))/H; 
	
	return((*colliderMatrix)[j+i1*w]==true||
		(*colliderMatrix)[j1+i1*w]==true);
}
bool Raupe::CollisionFrontR(){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int i=(int)((double)(h*yf+1))/H; 
	int j1=(int)((double)(w*(xf+Twidth)))/W;
	int i1=(int)((double)(h*(yf+Thight-1)))/H; 
	
	return ((*colliderMatrix)[j1+i*w]==true||
		(*colliderMatrix)[j1+i1*w]==true);
}
bool Raupe::CollisionFrontL(){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*xf-1))/W;//Why -1?
	int i=(int)((double)(h*yf+1))/H; 
	int i1=(int)((double)(h*(yf+Thight-1)))/H; 
	
	return((*colliderMatrix)[j+i*w]==true||
		(*colliderMatrix)[j+i1*w]==true);
}
/*collision back*/
bool Raupe::CollisionBack(){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*xb))/W;
	int i=(int)((double)(h*yb))/H; 
	int j1=(int)((double)(w*(xb+Twidth)))/W;
	int i1=(int)((double)(h*(yb+Thight)))/H; 
	
	if((*colliderMatrix)[j+i*w]==true||
		(*colliderMatrix)[j1+i*w]==true||
		(*colliderMatrix)[j+i1*w]==true||
		(*colliderMatrix)[j1+i1*w]==true			
			){
		return true;
	}else{
		return false;
	}
}
bool Raupe::CollisionBackU(){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*xb+1))/W;
	int i=(int)((double)(h*yb-1))/H;//why -1
	int j1=(int)((double)(w*(xb+Twidth-1)))/W;
	
	return((*colliderMatrix)[j+i*w]==true||
		(*colliderMatrix)[j1+i*w]==true);
}
bool Raupe::CollisionBackD(){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*xb+1))/W;
	int j1=(int)((double)(w*(xb+Twidth-1)))/W;
	int i1=(int)((double)(h*(yb+Thight)))/H; 
	
	return((*colliderMatrix)[j+i1*w]==true||
		(*colliderMatrix)[j1+i1*w]==true);
}
bool Raupe::CollisionBackR(){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int i=(int)((double)(h*yb+1))/H; 
	int j1=(int)((double)(w*(xb+Twidth)))/W;
	int i1=(int)((double)(h*(yb+Thight-1)))/H; 
	
	return ((*colliderMatrix)[j1+i*w]==true||
		(*colliderMatrix)[j1+i1*w]==true);
}
bool Raupe::CollisionBackL(){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*xb-1))/W;//Why -1?
	int i=(int)((double)(h*yb+1))/H; 
	int i1=(int)((double)(h*(yb+Thight-1)))/H; 
	
	return((*colliderMatrix)[j+i*w]==true||
		(*colliderMatrix)[j+i1*w]==true);
}
/*collision body parts*/
bool Raupe::Collision(int parti){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*x[parti]))/W;
	int i=(int)((double)(h*y[parti]))/H; 
	int j1=(int)((double)(w*(x[parti]+Twidth)))/W;
	int i1=(int)((double)(h*(y[parti]+Thight)))/H; 
	
	if((*colliderMatrix)[j+i*w]==true||
		(*colliderMatrix)[j1+i*w]==true||
		(*colliderMatrix)[j+i1*w]==true||
		(*colliderMatrix)[j1+i1*w]==true			
			){
		return true;
	}else{
		return false;
	}
}
bool Raupe::CollisionU(int parti){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*x[parti]+1))/W;
	int i=(int)((double)(h*y[parti]-1))/H;//why -1
	int j1=(int)((double)(w*(x[parti]+Twidth-1)))/W;
	
	return((*colliderMatrix)[j+i*w]==true||
		(*colliderMatrix)[j1+i*w]==true);
}
bool Raupe::CollisionD(int parti){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*x[parti]+1))/W;
	int j1=(int)((double)(w*(x[parti]+Twidth-1)))/W;
	int i1=(int)((double)(h*(y[parti]+Thight)))/H; 
	
	return((*colliderMatrix)[j+i1*w]==true||
		(*colliderMatrix)[j1+i1*w]==true);
}
bool Raupe::CollisionR(int parti){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int i=(int)((double)(h*y[parti]+1))/H; 
	int j1=(int)((double)(w*(x[parti]+Twidth)))/W;
	int i1=(int)((double)(h*(y[parti]+Thight-1)))/H; 
	
	return ((*colliderMatrix)[j1+i*w]==true||
		(*colliderMatrix)[j1+i1*w]==true);
}
bool Raupe::CollisionL(int parti){	
	int h=colliderMatrixH;
	int w=colliderMatrixW;
	double H=((double)(*WindowHight));
	double W=((double)(*WindowWidth));
	int j=(int)((double)(w*x[parti]-1))/W;//Why -1?
	int i=(int)((double)(h*y[parti]+1))/H; 
	int i1=(int)((double)(h*(y[parti]+Thight-1)))/H; 
	
	return((*colliderMatrix)[j+i*w]==true||
		(*colliderMatrix)[j+i1*w]==true);
}
void Raupe::DetermineXBodyPart(int i){
	double temp=(double) ((xf-xb)*(i+1));
	temp=temp/((double)(length+1));
	int xpos=(int)temp;
	x[i]=xb+xpos;

	//return xb+xpos;
}
void Raupe::DetermineYBodyPart(int i){
	bool ydetermined=false;
	if(i>length/2 && CollisionD(i) && y[i]>=yf && !movingUp){
		ydetermined=true;
		while(CollisionD(i) && movingDown && y[i]>yf){
			y[i]--;
		}
	}//else{
	//double alpha=atan(((double) (yb-yf))/
	//		((double) (xb-xf)));
	double pixellength=	
		sqrt((double) ((xb-xf)*(xb-xf)
					+(yb-yf)*(yb-yf)));
	double maxPixellength=((double) (Twidth*(length)));
	double ybToyf=(double) (yf-yb);
	double rela = pixellength/maxPixellength;

	double temp=((double) (i+1))*copysign(pixellength,(double) (xf-xb));
	temp=temp/((double)(length+1));
	//double xpos=temp;

	double temp1 = (-1.0)*cos(((double) (i+1))*2.0*M_PI/
			((double)(length+1)))+1.0;
	double temp2 = (-1.0)*ybToyf*(((double)(i+1))/((double) (length+1)));
	double ypos=((1.0-rela)*20.0*temp1+temp2);
	//double ypos=(1.0-rela)*20.0*temp1;

	//double xp=cos(alpha)*xpos+sin(alpha)*ypos;
	//double yp=cos(alpha)*ypos-sin(alpha)*xpos;


	if(!ydetermined){
		y[i]=yb-(int) (ypos+1.0);
		while(CollisionU(i)){
			y[i]++;
		}
	}
	//x[i]=xb+(int) (xp+1.0);
	//}


	//return yb-(int) ypos;
}
void Raupe::render(SDL_Renderer *renderer)
{
	// The actual y coordinates for some reason I must find out
	int axf=(int)((*ScaleL)*((double)xf))+*OffsetLx;
	int axb=(int)((*ScaleL)*((double)xb))+*OffsetLx;
	//Set rendering space and render to screen
	int ayf=(int)((*ScaleL)*((double)yf))+*OffsetLy;
	int ayb=(int)((*ScaleL)*((double)yb))+*OffsetLy;

	SDL_Rect renderQuadTar = {
		axb,
		ayb, 
		(int) (1.0+(*ScaleL)*(double)Twidth), 
		(int) (1.0+(*ScaleL)*(double)Thight)};

		//Render to screen
	SDL_RenderCopy(renderer,TailTexture,NULL,&renderQuadTar);
	
	for(int i=0;i<length;i++){
		DetermineXBodyPart(i);
		DetermineYBodyPart(i);
		/*int axi=(int)
			((*ScaleL)*
			 (double)(DetermineXBodyPart(i)))+*OffsetLx;*/
		int axi=(int)
			((*ScaleL)*
			 (double)(x[i]))+*OffsetLx;

		/*int ayi=(int)
			((*ScaleL)*((double)(DetermineYBodyPart(i))))+*OffsetLy;*/
		int ayi=(int)
			((*ScaleL)*
			 ((double)(y[i])))+*OffsetLy;


		renderQuadTar.x = axi;
		renderQuadTar.y = ayi;
		SDL_RenderCopy(renderer,BodyPartTexture,
				NULL,&renderQuadTar);
	}
	renderQuadTar.x = 
		axf;
	renderQuadTar.y =
		ayf;
	SDL_RenderCopy(renderer,HeadTexture,NULL,&renderQuadTar);
}
void Raupe::move(){
	bool nhcL=false;
	bool nhcR=false;
	bool nhcU=false;
	bool nhcD=false;//not head collisions
	for (int i=0;i<length;i++){
		nhcL=nhcL||CollisionL(i);
		nhcR=nhcR||CollisionR(i);
		nhcU=nhcU||CollisionU(i);
		nhcD=nhcD||CollisionD(i);
	}
	if(nhcR){
		movingLtR=false;
	}
	if(nhcL){
		movingRtL=false;
	}
	if(nhcD){
		movingDown=false;
	}
	if(nhcU){
		movingUp=false;
	}
	if (CollisionFrontD() && yf==yb && !userswitched){
		moveraup();
	}else if(!CollisionFrontD()){
		movehead();
	}else if(movingLtR){
		movetailLtR();
	}else if(movingRtL){
		movetailRtL();
	}else{
		movehead();
	}
}
void Raupe::movetailLtR(){

	if(framesWaited<velocity){
		framesWaited++;
	}else{
		framesWaited=0;
		int xtemp=x[0];
		int ytemp=y[0];
		
		double maxPixellength=(double) (Twidth*(length));
		x[0]=xf-maxPixellength/5;
		y[0]=yf;
		if(CollisionD(0) && xf>xb){
			
			if(yb>yf){
				yb--;			
			}else if(xb<=x[0]){
				xb++;
			}else if(yb<yf){
				yb++;
			}else{
				userswitched=false;
			}
	
		}
		x[0]=xtemp;
		y[0]=ytemp;
	}
}
void Raupe::movetailRtL(){

	if(framesWaited<velocity){
		framesWaited++;
	}else{
		framesWaited=0;
		int xtemp=x[0];
		int ytemp=y[0];
		
		double maxPixellength=(double) (Twidth*(length));
		x[0]=xf+maxPixellength/5;
		y[0]=yf;
		if(CollisionD(0) && xb>xf){
			
			if(yb>yf){
				yb--;			
			}else if(xb>=x[0]){
				xb--;
			}else if(yb<yf){
				yb++;
			}else{
				userswitched=false;
			}
	
		}
		x[0]=xtemp;
		y[0]=ytemp;
	}
}
void Raupe::movehead(){
	double pixellength=sqrt((double) ((xb-xf)*(xb-xf)+(yb-yf)*(yb-yf)));
	double maxPixellength=(double) (Twidth*(length));
	bool notbroken=true;
	int ymax=*WindowHight;
	for(int i=0;i<length;i++){
		if(y[i]<ymax){
			ymax=y[i];
		}
	}
	if (ymax-Thight/2>yf){
		notbroken=false;
	}

	CollisionFront();

	if (framesWaited<velocity){
		framesWaited++;
	}else if(notbroken || !movingUp)
	{
		framesWaited=0;
		/*This is to prevent the case, if we
		 * change directions while the back is
		 * moving. In that case we should switch
		 * to front moving*/
		/*move up or down head, until the Raupe is stretched
		 * if the Raupe is already stretched but in the
		 * other direction, we should also move*/
		if(movingUp && !CollisionFrontU()
				&& (pixellength<=maxPixellength
				|| yf>yb))
		{
			yf--;
		}
		else if(movingDown && !CollisionFrontD()
				&& (pixellength<=maxPixellength
				|| yf<yb))
		{
			yf++;
		}
		/*moving from left to right*/
		else if(movingLtR && !CollisionFrontR() //&& xf>xb
				&& ((pixellength<maxPixellength)
					|| xf<xb))
		{
			xf++;
		}
		
		/*moving from right to left. the same*/
		else if(movingRtL && !CollisionFrontL() //&& xf<xb
				&& 
				((pixellength<maxPixellength)
					|| xf>xb))
		{
			xf--;
		}
	}
}
void Raupe::moveraup(){
	double pixellength=sqrt((double) ((xb-xf)*(xb-xf)+(yb-yf)*(yb-yf)));
	double maxPixellength=(double) (Twidth*(length));

	CollisionFront();
	/*If we have not waited velocity frames there is nothing to do*/
	if(framesWaited<velocity)
	{
		framesWaited++;
	}
	else
	{
		framesWaited=0;
		/*This is to prevent the case, if we
		 * change directions while the back is
		 * moving. In that case we should switch
		 * to front moving*/
		if(movingLtR && (xb-xf)>=0){
			movingf=true;

		}else if(movingRtL && (xf-xb)>=0){
			movingf=true;
		}
		/*move up or down head, until the Raupe is stretched
		 * if the Raupe is already stretched but in the
		 * other direction, we should also move*/
		if(movingUp &&!CollisionFrontU()
				&& (pixellength<=maxPixellength
				|| yf>yb))
		{
			yf--;
		}
		else if(movingDown && !CollisionFrontD()
				&& (pixellength<=maxPixellength
				|| yf<yb))
		{
			yf++;
		}
		/*moving from left to right*/
		else if(movingLtR)
		{
			/*we move the head until the Raupe
			 * is stretched and then we move
			 * the back until there is a nice
			 * small distance between head and tail,
			 * then we move the head again*/
			/*we want to also move if the Raupe is
			 * stretched but we change directions of 
			 * moving*/
			if(movingf)
			{
				if(pixellength>=maxPixellength &&
						xf>xb)
				{
					movingf=false;
				}
				else
				{
					if(CollisionFrontR()){
						movingf=false;
					}else{
						xf++;
					}
				}
			}
			else
			{
				if(pixellength<maxPixellength/5)
				{
					movingf=true;
				}
				else
				{
					xb++;
				}
			}
		}
		/*moving from right to left. the same*/
		else if(movingRtL)
		{
			if(movingf)
			{
				if(pixellength>=maxPixellength &&
						xf<xb)
				{
					movingf=false;
				}
				else
				{
					if(CollisionFrontL()){
						movingf=false;
					}else{
						xf--;
					}
				}
			}
			else
			{
				if(pixellength<maxPixellength/5)
				{
					movingf=true;
				}
				else
				{
					xb--;
				}
			}
		}
	}
}
int Raupe::getframesWaited(){
	return framesWaited;
}
int Raupe::getxf(){
	return xf;
}
int Raupe::getyf(){
	return yf;
}
int Raupe::getxb(){
	return xb;
}
int Raupe::getyb(){
	return yb;
}
void Raupe::setx(int x){
	xf=x+40;
	xb=x;
}
void Raupe::sety(int y){
	yf=y;
	yb=y;
}
int Raupe::getThight(){
	return Thight;
}
void Raupe::setcolliderMatrix(std::vector<bool> *colliderM,int W, int H){
	colliderMatrix=colliderM;
	colliderMatrixW=W;
	colliderMatrixH=H;
	
}
