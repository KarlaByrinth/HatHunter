#include "App.hpp"

int main(int argc, char* argv[]) {
	App theApp;
	if (!theApp.OnExecute()){
		return 1;
	}
	return 0;
}
