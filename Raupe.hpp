#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <cmath>
#include <iostream>
#include <vector>
#include <cstdlib>
//#include "Level.hpp"

class Raupe{
	private:
	int xb, yb; // coordinates of the back of the Raupe
	int xf, yf; // coordinates of the front of the Raupe
	int *x, *y; // coordinates of the body parts
	int length; // number of peals aka bodyParts between head and tail
	int velocity; // velocity of movement, frames to wait until next
	//movement
	int framesWaited;

	std::vector<bool> *colliderMatrix;
	int colliderMatrixW;
	int colliderMatrixH;


	SDL_Texture* HeadTexture;
	SDL_Texture* TailTexture;
	SDL_Texture* BodyPartTexture;

	int Twidth; // Width and Hight of Texture
	int Thight;

	int *WindowHight;//pointer to window hight and width
	int *WindowWidth;

	double *ScaleL;
	int *OffsetLx;
	int *OffsetLy;

	bool LoadHeadTexture(SDL_Renderer *renderer);
	bool LoadBodyPartTexture(SDL_Renderer *renderer);
	bool LoadTailTexture(SDL_Renderer *renderer);
	void LoadTexture(SDL_Renderer *renderer);
	void DetermineXBodyPart(int i);
	void DetermineYBodyPart(int i);
	void moveraup();
	void movehead();
	void movetailLtR();
	void movetailRtL();
	
	public:

	bool CollisionFront();
	bool CollisionFrontL();
	bool CollisionFrontR();
	bool CollisionFrontU();
	bool CollisionFrontD();
	bool CollisionBack();
	bool CollisionBackL();
	bool CollisionBackR();
	bool CollisionBackU();
	bool CollisionBackD();
	bool Collision(int parti);
	bool CollisionL(int parti);
	bool CollisionR(int parti);
	bool CollisionU(int parti);
	bool CollisionD(int parti);

	bool movingLtR; // true if the Raupe is moving from left to right
	bool movingRtL; // true if the Raupe is moving from right to left
	bool movingf; // true if the part to be moved next is the front
	bool movingUp; // true if the Raupe is moving its head up
	bool movingDown; // true if the Raupe is moving its head down
	bool userswitched; // if the user decided the tail should rather move than the head

	Raupe(SDL_Renderer *renderer,
			double *LevelScale,
			int *Offsetx, int *Offsety,
			int *WindowW, int *WindowH);
	~Raupe();
	Raupe(SDL_Renderer *renderer,
			double *LevelScale, 
			int *WindowW, int *WindowH,
			int *Offsetx, int *Offsety,
			int x1, int y1, int x2, int y2, int l);
	void free();
	void render(SDL_Renderer *renderer);
	void move();
	int getframesWaited();
	int getxf();
	int getyf();
	int getxb();
	int getyb();
	int getThight();
	void setx(int x);
	void sety(int y);
	void setcolliderMatrix(std::vector<bool> *colliderM,int W,int H);
};
